# SnowSeeker

SnowSeeker is an app to let users browse ski resorts around the world, to help them find one suitable for their next holiday.

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/snowseeker-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- Split view controllers
- Binding an alert to an optional
- Using groups for flexible layout
- NavigationSplitView
- searchable()
