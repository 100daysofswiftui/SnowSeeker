//
//  ContentView.swift
//  SnowSeeker
//
//  Created by Pascal Hintze on 25.03.2024.
//

import SwiftUI

struct ContentView: View {
    let resorts: [Resort] = Bundle.main.decode("resorts.json")

    @State private var favorites = Favorites()
    @State private var searchText = ""

    var filteredResorts: [Resort] {
        if searchText.isEmpty {
            resorts
        } else {
            resorts.filter { $0.name.localizedStandardContains(searchText) }
        }
    }

    @State private var sortOrder: SortType = .defaultOrder

    enum SortType {
        case defaultOrder, alphabeticalOrder, countryOrder
    }

    var sortedResorts: [Resort] {
        switch sortOrder {
        case .defaultOrder:
            filteredResorts
        case .alphabeticalOrder:
            filteredResorts.sorted { $0.name < $1.name }
        case .countryOrder:
            filteredResorts.sorted { $0.country < $1.country }
        }
    }

    var body: some View {
        NavigationSplitView {
            List(sortedResorts) { resort in
                NavigationLink(value: resort) {
                    HStack {
                        Image(resort.country)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 40, height: 25)
                            .clipShape(.rect(cornerRadius: 5))
                            .overlay(
                                RoundedRectangle(cornerRadius: 5)
                                    .stroke(.black, lineWidth: 1)
                            )

                        VStack(alignment: .leading) {
                            Text(resort.name)
                                .font(.headline)

                            Text("\(resort.runs) runs")
                                .foregroundStyle(.secondary)
                        }

                        if favorites.contains(resort) {
                            Spacer()

                            Image(systemName: "heart.fill")
                                .accessibilityLabel("This is a favorite resort")
                                .foregroundStyle(.red)
                        }
                    }
                }
            }
            .toolbar {
                Menu {
                    Button("Default") {
                        sortOrder = .defaultOrder
                    }
                    Button("Alphabetical") {
                        sortOrder = .alphabeticalOrder
                    }
                    Button("Country") {
                        sortOrder = .countryOrder
                    }

                } label: {
                    Label("Sort by", systemImage: "arrow.up.arrow.down")
                }
            }

            .navigationTitle("Resorts")
            .navigationDestination(for: Resort.self) { resort in
                ResortView(resort: resort)
            }
            .searchable(text: $searchText, prompt: "Search for a resort")
        } detail: {
            WelcomeView()
        }
        .environment(favorites)
    }
}

#Preview {
    ContentView()
}
