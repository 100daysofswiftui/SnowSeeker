//
//  SnowSeekerApp.swift
//  SnowSeeker
//
//  Created by Pascal Hintze on 25.03.2024.
//

import SwiftUI

@main
struct SnowSeekerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
